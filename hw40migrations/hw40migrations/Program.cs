﻿using hw40migrations.Models;
using System;

namespace hw40migrations
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new ApplicationDbContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    var user = new User
                    {
                        Login = "abc",
                        Name = "A",
                        Surname = "B",
                        DateOfBirth = DateTime.Now,
                        DateOfCreation = DateTime.Now
                    };

                    db.Users.Add(user);
                    db.SaveChanges();

                    var post = new Post
                    {
                        Comment = "New post",
                        UserID = user.ID,
                        PostID = 1
                    };

                    db.Posts.Add(post);
                    db.SaveChanges();

                    transaction.Commit();
                }
            }
        }
    }
}
