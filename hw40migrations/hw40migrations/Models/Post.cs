﻿using System;
using System.Collections.Generic;
using System.Text;

namespace hw40migrations.Models
{
    public class Post
    {
        public int PostID { get; set; }
        public int UserID { get; set; }
        public string Comment { get; set; }
    }
}
