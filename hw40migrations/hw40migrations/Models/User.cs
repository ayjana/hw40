﻿using System;
using System.Collections.Generic;
using System.Text;

namespace hw40migrations.Models
{
    public class User
    {
        public int ID { get; set; }
        public string Login { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime DateOfCreation { get; set; }
    }
}
